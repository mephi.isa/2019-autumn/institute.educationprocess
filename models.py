class Chair:
    def __init__(self, name):
        self.name = name
        self.teachers = []
        self.specs = []

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def add_teacher(self, teacher_obj):
        self.teachers.append(teacher_obj)

    def delete_teacher(self, teacher_obj):
        self.teachers.remove(teacher_obj)

    def add_spec(self, spec_obj):
        self.specs.append(spec_obj)

    def delete_spec(self, spec_obj):
        self.specs.remove(spec_obj)

    def search_teacher(self, teacher_obj):
        for i in self.teachers:
            if i == teacher_obj:
                return True
        return False

    def search_spec(self, spec_obj):
        for i in self.specs:
            if i == spec_obj:
                return True
        return False

    def __eq__(self, other):
        if self.name == other.name:
            return True
        else:
            return False


    def __str__(self):
        return self.name

class Spec:
    def __init__(self, name):
        self.name = name
        self.subjects = []
        self.groups = []

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def add_subject(self, subject_obj):
        self.subjects.append(subject_obj)

    def delete_subject(self, subject_obj):
        self.subjects.remove(subject_obj)

    def add_group(self, group_obj):
        self.groups.append(group_obj)

    def delete_group(self, group_obj):
        self.groups.remove(group_obj)

    def search_subject(self, subject_obj):
        for i in self.subjects:
            if i == subject_obj:
                return True
        return False

    def search_group(self, group_obj):
        for i in self.groups:
            if i == group_obj:
                return True
        return False


    def __str__(self):
        return self.name

class Subject:
    def __init__(self, name, description, type_subject, number_modules):
        self.name = name
        self.description = description
        self.type_subject = type_subject
        self.number_modules = number_modules
        if self.number_modules == 1:
            self.modules = [100 // number_modules] * number_modules
        else:
            self.modules = [100 // number_modules] * (number_modules - 1)
            self.modules.append(100 // number_modules + 100 % number_modules)

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_description(self, description):
        self.description = description

    def get_description(self):
        return self.description

    def set_type_subject(self, type_subject):
        self.type_subject = type_subject

    def get_type_subject(self):
        return self.type_subject

    def set_number_modules(self, number_modules):
        self.number_modules = number_modules
        if self.number_modules == 1:
            self.modules = [100 // number_modules] * number_modules
        else:
            self.modules = [100 // number_modules] * (number_modules - 1)
            self.modules.append(100 // number_modules + 100 % number_modules)

    def get_number_modules(self):
        return self.number_modules

    def add_module(self, module_obj):
        self.modules.append(module_obj)

    def delete_module(self, module_obj):
        self.modules.remove(module_obj)

    def set_module(self, id_module, max_rating):
        if self.check_total_rating(id_module, max_rating):
            self.modules[id_module] = max_rating
            return self.modules[id_module]
        else:
            return False

    def get_module(self, id_module):
        return self.modules[id_module]

    def check_total_rating(self, id_module, max_rating):
        if sum(self.modules) - self.modules[id_module] + max_rating <= 100:
            return True
        else:
            return False

    def check_module(self, id_module, rating):
        if rating <= self.modules[id_module]:
            return True
        else:
            return False


    def __str__(self):
        return self.name

class Group:
    def __init__(self, name, year):
        self.name = name
        self.year = year
        self.students = []

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_year(self, year):
        self.year = year

    def get_year(self):
        return self.year

    def add_student(self, student_obj):
        self.students.append(student_obj)

    def delete_student(self, student_obj):
        self.students.remove(student_obj)

    def search_student(self, student_obj):
        for i in self.students:
            if i == student_obj:
                return True
        return False


    def __str__(self):
        return self.name

class Teacher:
    def __init__(self, name):
        self.name = name
        self.subjects = []

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def add_subject(self, subject_obj):
        self.subjects.append(subject_obj)

    def delete_subject(self, subject_obj):
        self.subjects.remove(subject_obj)

    def search_subject(self, subject_obj):
        for i in self.subjects:
            if i == subject_obj:
                return True
        return False


    def __str__(self):
        return self.name

class Mark:
    def __init__(self, subject, mark, mdate, name_teacher, id_module):
        self.subject = subject
        self.mark = mark
        self.mdate = mdate
        self.name_teacher = name_teacher
        self.id_module = id_module

    def set_subject(self, subject):
        self.subject = subject

    def get_subject(self):
        return self.subject

    def set_mark(self, mark):
        self.mark = mark

    def get_mark(self):
        return self.mark

    def set_mdate(self, mdate):
        self.mdate = mdate

    def get_mdate(self):
        return self.mdate

    def set_name_teacher(self, name_teacher):
        self.name_teacher = name_teacher

    def get_name_teacher(self):
        return self.name_teacher

    def set_id_module(self, id_module):
        self.id_module = id_module

    def get_id_module(self):
        return self.id_module


    def __str__(self):
        return "%d %s %s"%(self.mark, self.mdate, self.name_teacher)

class Student:
    def __init__(self, name):
        self.name = name
        self.marks = []

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def add_mark(self, mark_obj, subject_obj):
        if self.check_mark(mark_obj, subject_obj):
            self.marks.append(mark_obj)

    def delete_mark(self, mark_obj):
        self.marks.remove(mark_obj)

    def check_mark(self, mark_obj, subject_obj):
        return subject_obj.check_module(mark_obj.get_id_module(), mark_obj.get_mark())

    def search_mark(self, mark_obj):
        for i in self.marks:
            if i == mark_obj:
                return True
        return False


    def __str__(self):
        return self.name