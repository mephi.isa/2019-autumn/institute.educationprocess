import unittest
import models

class TestChair(unittest.TestCase):
    def setUp(self):
        self.ch = models.Chair("Chair")

    def test_set_get_name(self):
        self.ch.set_name("RK")
        self.assertEqual("RK", self.ch.get_name())

    def test_add_delete_search_teacher(self):
        teach1 = models.Teacher("Igor Akinfeev")
        teach2 = models.Teacher("Igor Diveev")
        self.ch.add_teacher(teach1)
        self.assertTrue(self.ch.search_teacher(teach1))
        self.assertFalse(self.ch.search_teacher(teach2))
        self.ch.delete_teacher(teach1)
        self.assertFalse(self.ch.search_teacher(teach1))

    def test_add_delete_search_spec(self):
        spect1 = models.Spec("IT")
        spect2 = models.Spec("EC")
        self.ch.add_spec(spect1)
        self.assertTrue(self.ch.search_spec(spect1))
        self.assertFalse(self.ch.search_spec(spect2))
        self.ch.delete_spec(spect1)
        self.assertFalse(self.ch.search_spec(spect1))

class TestSpec(unittest.TestCase):
    def setUp(self):
        self.spect = models.Spec("Spec")

    def test_set_get_name(self):
        self.spect.set_name("IT")
        self.assertEqual("IT", self.spect.get_name())

    def test_add_delete_search_subject(self):
        sub1 = models.Subject("C", "The course is about developing with the use of C", "Ex", 3)
        sub2 = models.Subject("PHP", "The course is about developing with the use of PHP", "Ex", 3)
        self.spect.add_subject(sub1)
        self.assertTrue(self.spect.search_subject(sub1))
        self.assertFalse(self.spect.search_subject(sub2))
        self.spect.delete_subject(sub1)
        self.assertFalse(self.spect.search_subject(sub1))

    def test_add_delete_search_group(self):
        gr1 = models.Group("M102", 20)
        gr2 = models.Group("M122", 20)
        self.spect.add_group(gr1)
        self.assertTrue(self.spect.search_group(gr1))
        self.assertFalse(self.spect.search_group(gr2))
        self.spect.delete_group(gr1)
        self.assertFalse(self.spect.search_group(gr1))

class TestSubject(unittest.TestCase):
    def setUp(self):
        self.sub = models.Subject("Subject", "Description", "Type_subject", 1)

    def test_set_get_name(self):
        self.sub.set_name("PHP")
        self.assertEqual("PHP", self.sub.get_name())

    def test_set_get_description(self):
        self.sub.set_description("The course is about developing with the use of PHP")
        self.assertEqual("The course is about developing with the use of PHP", self.sub.get_description())

    def test_set_get_type_subject(self):
        self.sub.set_type_subject("Ex")
        self.assertEqual("Ex", self.sub.get_type_subject())

    def test_set_get_number_modules(self):
        self.sub.set_number_modules(3)
        self.assertEqual(3, self.sub.get_number_modules())

    def test_set_module(self):
        self.sub.set_number_modules(2)
        self.id_module = 1
        self.max_rating1 = 35
        self.max_rating2 = 55
        self.assertEqual(self.sub.set_module(self.id_module, self.max_rating1), self.sub.get_module(self.id_module))
        self.assertNotEqual(self.sub.set_module(self.id_module, self.max_rating2), self.sub.get_module(self.id_module))

    def test_check_total_rating(self):
        self.sub.set_number_modules(2)
        self.id_module = 1
        self.max_rating1 = 35
        self.max_rating2 = 55
        self.assertTrue(self.sub.check_total_rating(self.id_module, self.max_rating1))
        self.assertFalse(self.sub.check_total_rating(self.id_module, self.max_rating2))

    def test_check_module(self):
        self.sub.set_number_modules(2)
        self.id_module = 1
        self.rating1 = 35
        self.rating2 = 55
        self.assertTrue(self.sub.check_module(self.id_module, self.rating1))
        self.assertFalse(self.sub.check_module(self.id_module, self.rating2))

class TestGroup(unittest.TestCase):
    def setUp(self):
        self.gr = models.Group("Group", 20)

    def test_set_get_name(self):
        self.gr.set_name("M102")
        self.assertEqual("M102", self.gr.get_name())

    def test_set_get_year(self):
        self.gr.set_year(19)
        self.assertEqual(19, self.gr.get_year())

    def test_add_delete_search_student(self):
        stud1 = models.Student("Alexandr Golovin")
        stud2 = models.Student("Georgiy Schennikov")
        self.gr.add_student(stud1)
        self.assertTrue(self.gr.search_student(stud1))
        self.assertFalse(self.gr.search_student(stud2))
        self.gr.delete_student(stud1)
        self.assertFalse(self.gr.search_student(stud1))

class TestTeacher(unittest.TestCase):
    def setUp(self):
        self.teach = models.Teacher("Teacher")

    def test_set_get_name(self):
        self.teach.set_name("Igor Akinfeev")
        self.assertEqual("Igor Akinfeev", self.teach.get_name())

    def test_add_delete_search_subject(self):
        sub1 = models.Subject("C", "The course is about developing with the use of C", "Ex", 3)
        sub2 = models.Subject("PHP", "The course is about developing with the use of PHP", "Ex", 3)
        self.teach.add_subject(sub1)
        self.assertTrue(self.teach.search_subject(sub1))
        self.assertFalse(self.teach.search_subject(sub2))
        self.teach.delete_subject(sub1)
        self.assertFalse(self.teach.search_subject(sub1))

class TestMark(unittest.TestCase):
    def setUp(self):
        self.m = models.Mark("Subject", 30, "25/11/19", "Teacher", 1)

    def test_set_get_subject(self):
        self.m.set_subject("Igor Akinfeev")
        self.assertEqual("Igor Akinfeev", self.m.get_subject())

    def test_set_get_mark(self):
        self.m.set_subject("Igor Akinfeev")
        self.assertEqual("Igor Akinfeev", self.m.get_subject())

    def test_set_get_mdate(self):
        self.m.set_mdate("Igor Akinfeev")
        self.assertEqual("Igor Akinfeev", self.m.get_mdate())

    def test_set_get_name_teacher(self):
        self.m.set_name_teacher("Igor Akinfeev")
        self.assertEqual("Igor Akinfeev", self.m.get_name_teacher())

    def test_set_get_id_module(self):
        self.m.set_id_module("Igor Akinfeev")
        self.assertEqual("Igor Akinfeev", self.m.get_id_module())

class TestStudent(unittest.TestCase):
    def setUp(self):
        self.stud = models.Student("Student")

    def test_set_get_name(self):
        self.stud.set_name("Alexandr Golovin")
        self.assertEqual("Alexandr Golovin", self.stud.get_name())

    def test_add_delete_search_mark(self):
        sub1 = models.Subject("PHP", "The course is about developing with the use of PHP", "Ex", 3)
        sub2 = models.Subject("C", "The course is about developing with the use of C", "Ex", 2)
        m1 = models.Mark("PHP", 30, "25/11/19", "Igor Akinfeev", 1)
        m2 = models.Mark("C", 50, "25/11/19", "Igor Akinfeev", 1)
        self.stud.add_mark(m1, sub1)
        self.assertTrue(self.stud.search_mark(m1))
        self.assertFalse(self.stud.search_mark(m2))
        self.stud.delete_mark(m1)
        self.assertFalse(self.stud.search_mark(m1))

    def test_check_mark(self):
        sub = models.Subject("PHP", "The course is about developing with the use of PHP", "Ex", 3)
        m = models.Mark("PHP", 30, "25/11/19", "Igor Akinfeev", 1)
        self.assertTrue(self.stud.check_mark(m, sub))
