import datetime
import tkinter as tk
from tkinter import ttk
import psycopg2
from tkinter import messagebox

connection = psycopg2.connect(
  database="AIS",
  user="postgres",
  password="dastan",
  host="127.0.0.1",
  port="5432"
)
cursor = connection.cursor()

class GenericError(Exception):

    def __init__(self, message):
        super().__init__(message)

#Авроризация
class Main(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.init_main()

    def init_main(self):
        self.login = tk.StringVar()
        self.password = tk.StringVar()

        self.label_1 = tk.Label(root, text="Ведите логин: ", fg="black", width=30).place(x=30, y=50)
        self.entry_2 = ttk.Entry(root, textvar=self.login, width=30).place(x=180, y=50)

        self.label_2 = tk.Label(root, text="Ведите пароль: ", fg="black", width=30).place(x=25, y=80)
        self.entry_2 = ttk.Entry(root, textvar=self.password, width=30).place(x=180, y=80)

        self.btn_1 = tk.Button(root, text="Войти", width="20", command=self.validate, background="royalblue",
                               foreground="white", font="15").place(x=150, y=110)

        self.btn_1 = tk.Button(root, text="Студент", width="20", command=self.stud, background="royalblue",
                               foreground="white", font="15").place(x=150, y=160)

    def validate(self):
         self.log = self.login.get()
         self.pas = self.password.get()
         cursor.execute("select * from users where login='" + self.log + "'")
         self.users = cursor.fetchone()
         print(self.users)
         if self.users[0] and self.users[1] == self.pas and self.users[2] == "admin":
             root.destroy()
             AdminList()
         elif self.users[0] and self.users[1] == self.pas and self.users[2] == "cathedra":
             root.destroy()
             TeachersList()
             GroupList()
             UserStudent()
             Itemslist()

         elif self.users[0] and self.users[1] == self.pas and self.users[2] == "teacher":
             root.destroy()
             GroupList()
             Contorl()
         else:
            messagebox.showinfo("Error", "Неверные данные")

    def stud(self):
        UserStudent()

#Меню Админа
class AdminList(tk.Toplevel):


    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("AdminList")
        self.geometry("500x500+300+100")

        self.tree = ttk.Treeview(self, columns=('Логин', 'Пароль', 'Роль'), height=15, show='headings')
        self.tree.column('Логин', width=100)
        self.tree.column('Пароль', width=100)
        self.tree.column('Роль', width=100)

        self.tree.heading('Логин', text='Логин')
        self.tree.heading('Пароль', text='Пароль')
        self.tree.heading('Роль', text='Роль')
        cursor.execute('select * from users')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0], record[i][1], record[i][2]))
        self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()
        self.btn_66 = tk.Button(self, text="Создать пользователя", width="20", command=self.create, background="royalblue",
                                foreground="white", font="15").place(x=150, y=430)

    def create(self):
        self.destroy()
        AdminFrame()

    def link_tree(self, event):
        tl = tk.Toplevel()
        tl.title("Select")
        tl.geometry("500x200+300+300")

        btn_66 = tk.Button(tl, text="Update", width="20", command=self.create, background="royalblue", foreground="white",
                           font="15").pack()
        btn_67 = tk.Button(tl, text="Delete", width="20", command=self.create, background="royalblue", foreground="white",
                           font="15").pack()

        curItem = self.tree.focus()
        id = self.tree.item(curItem)['values'][0]
        print(id)
#Создание пользователя
class AdminFrame(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Admin Panel")
        self.geometry("500x500+300+100")
        self.llogin = tk.StringVar()
        self.ppaswword = tk.StringVar()
        self.rrole = tk.StringVar()



        self.label_66 = tk.Label(self, text="Логин: ", fg="black", width=30).place(x=65, y=50)
        self.entry_66 = ttk.Entry(self, textvar=self.llogin, width=30).place(x=210, y=50)

        self.label_26 = tk.Label(self, text="Пароль: ", fg="black", width=30).place(x=60, y=80)
        self.entry_26 = ttk.Entry(self, textvar=self.ppaswword, width=30).place(x=210, y=80)


        self.rbtn_1 = tk.Radiobutton(self, text="Преподаватель", width="15", value="teacher", variable=self.rrole).place(x=50,y=120)
        self.rbtn_2 = tk.Radiobutton(self, text="Кафедра", width="10", value="cathedra", variable=self.rrole).place(x=200,y=120)
        self.rbtn_3 = tk.Radiobutton(self, text="Ректорат", width="10", value="rectorate", variable=self.rrole).place(x=350,y=120)

        self.btn_3 = tk.Button(self, text="Создать", width="20", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=150, y=430)

    def create(self):
        l = self.llogin.get()
        p = self.ppaswword.get()
        r = self.rrole.get()
        cursor.execute("insert into users (login,password,role) values('" + l + "','" + p + "','" + r + "')")
        connection.commit()
        self.destroy()
        AdminList()

# Менею
class Menu(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Формирование группы")
        self.geometry("500x170+300+100")

        self.btn_1 = tk.Button(self, text="Список групп", width="20", command=self.gr, background="royalblue",
                               foreground="white", font="15").place(x=10, y=10)

        self.btn_2 = tk.Button(self, text="Список назначений", width="20", command=self.na, background="royalblue",
                               foreground="white", font="15").place(x=260, y=10)

        self.btn_3 = tk.Button(self, text="Список преподавателей", width="20", command=self.tc, background="royalblue",
                               foreground="white", font="15").place(x=10, y=55)

        self.btn_4 = tk.Button(self, text="Список предметов", width="20", command=self.it, background="royalblue",
                               foreground="white", font="15").place(x=260, y=55)

        self.btn_4 = tk.Button(self, text="Контроль", width="20", command=self.co, background="royalblue",
                               foreground="white", font="15").place(x=150, y=110)

    def gr(self):
        self.destroy()
        GroupList()

    def na(self):
        self.destroy()
        AppointmentsList()

    def tc(self):
        self.destroy()
        TeachersList()

    def it(self):
        self.destroy()
        Itemslist()

    def co(self):
        self.destroy()
        Contorl()

# Список назначений
class AppointmentsList(tk.Toplevel):

    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Назначения")
        self.geometry("500x500+300+100")

        self.tree = ttk.Treeview(self, columns=('ID', 'Название предмета', 'Преподаватель'), height=15, show='headings')
        self.tree.column('ID', width=50)
        self.tree.column('Название предмета', width=200)
        self.tree.column('Преподаватель', width=200)

        self.tree.heading('ID', text='ID')
        self.tree.heading('Название предмета', text='Название предмета')
        self.tree.heading('Преподаватель', text='Преподаватель')
        cursor.execute('select назначение.id,преподаватель.фио,предмет.название from преподаватель join назначение on преподаватель.id = назначение.id_преподавателя join предмет on предмет.id=назначение.id_предмета')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0], record[i][1], record[i][2]))
        # self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()
        self.btn_3 = tk.Button(self, text="Списка преподавателей", width="20", command=self.tlist, background="royalblue",
                               foreground="white", font="15").place(x=10, y=350)

        self.btn_4 = tk.Button(self, text="Списка предметов", width="20", command=self.ilist, background="royalblue",
                               foreground="white", font="15").place(x=260, y=350)

        self.btn_5 = tk.Button(self, text="Новое назначение", width="20", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=150, y=400)

        self.btn_6 = tk.Button(self, text="Меню", width="20",command=self.up, background="royalblue",
                               foreground="white", font="15").place(x=150, y=440)

    def up(self):
        self.destroy()
        Menu()

    def ilist(self):
        Itemslist()

    def tlist(self):
        TeachersList()

    def create(self):
        self.destroy()
        Itemslist()
        TeachersList()
        AppointmentsFrame()


    # def link_tree(self, event):
    #     curItem = self.tree.focus()
    #     id = self.tree.item(curItem)['values'][0]
    #     print(id)
    #     EmployeeList(id)
    #     self.destroy()

# Добавление в список назначений
class AppointmentsFrame(tk.Toplevel):

    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Добавление назначения")
        self.geometry("500x500+300+100")
        self.idt = tk.StringVar()
        self.idi = tk.StringVar()

        self.label_1 = tk.Label(self, text="ID преподавателя: ", fg="black", width=30).place(x=40, y=50)
        self.entry_2 = ttk.Entry(self, textvar=self.idt, width=30).place(x=210, y=50)

        self.label_2 = tk.Label(self, text="ID предмета: ", fg="black", width=30).place(x=40, y=80)
        self.entry_2 = ttk.Entry(self, textvar=self.idi, width=30).place(x=210, y=80)

        self.btn_2 = tk.Button(self, text="Создать назначение", width="20", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=150, y=140)

    def create(self):
        self.t = self.idt.get()
        self.i = self.idi.get()
        cursor.execute(
            "insert into назначение (id_преподавателя, id_предмета) values('" + self.t + "','" + self.i + "')")
        connection.commit()
        answer = messagebox.askyesno("Назначение добавлен успешно", "Хотите продолжить ?")
        if answer == True:
            self.destroy()
            AppointmentsFrame()
        else:
            self.destroy()
            AppointmentsList()

# Запрос списка предметов
class Itemslist(tk.Toplevel):

    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Предметы")
        self.geometry("500x500+300+100")

        self.tree = ttk.Treeview(self, columns=('ID', 'Название предмета', 'Выделеное время'), height=15, show='headings')
        self.tree.column('ID', width=50)
        self.tree.column('Название предмета', width=200)
        self.tree.column('Выделеное время', width=200)

        self.tree.heading('ID', text='ID')
        self.tree.heading('Название предмета', text='Название предмета')
        self.tree.heading('Выделеное время', text='Выделеное время')
        cursor.execute('select*from предмет')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0], record[i][1], record[i][2]))
        # self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()
        self.btn_3 = tk.Button(self, text="Добавить предмет", width="20", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=150, y=400)

        self.btn_4 = tk.Button(self, text="Назад", width="20",command=self.up, background="royalblue",
                               foreground="white", font="15").place(x=150, y=440)

    def up(self):
        self.destroy()
        Menu()

    def create(self):
        self.destroy()
        ItemsFrame()

# Запрос списка преподавателей
class TeachersList(tk.Toplevel):

    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Списка преподавателей")
        self.geometry("500x500+300+100")

        self.tree = ttk.Treeview(self, columns=('ID', 'ФИО'), height=15, show='headings')
        self.tree.column('ID', width=50)
        self.tree.column('ФИО', width=300)

        self.tree.heading('ID', text='ID')
        self.tree.heading('ФИО', text='ФИО')
        cursor.execute('select*from преподаватель')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0], record[i][1]))
        # self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()
        self.btn_3 = tk.Button(self, text="Добавить преподавателя", width="30", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=100, y=400)

        self.btn_4 = tk.Button(self, text="Назад", width="30",command=self.up, background="royalblue",
                               foreground="white", font="15").place(x=100, y=440)

    def up(self):
        self.destroy()
        Menu()

    def create(self):
        self.destroy()
        TeachersFrame()


    # def link_tree(self, event):
    #     curItem = self.tree.focus()
    #     id = self.tree.item(curItem)['values'][0]
    #     print(id)
    #     EmployeeList(id)
    #     self.destroy()

# Добавление в список преподавателей
class TeachersFrame(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Добавление преподавателя")
        self.geometry("500x500+300+100")
        self.namet = tk.StringVar()

        self.label_1 = tk.Label(self, text="ФИО: ", fg="black", width=30).place(x=40, y=50)
        self.entry_2 = ttk.Entry(self, textvar=self.namet, width=40).place(x=180, y=50)

        self.btn_2 = tk.Button(self, text="Создать", width="20", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=150, y=140)

    def create(self):
        self.name = self.namet.get()
        cursor.execute(
            "insert into преподаватель (фио) values('" + self.name + "')")
        connection.commit()
        answer = messagebox.askyesno("Преподаватель добавлен успешно", "Хотите продолжить ?")
        if answer == True:
            self.destroy()
            TeachersFrame()
        else:
            self.destroy()
            TeachersList()

# Добавление в список предметов
class ItemsFrame(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Предметы")
        self.geometry("500x500+300+100")
        self.namei = tk.StringVar()
        self.timei = tk.StringVar()

        self.label_1 = tk.Label(self, text="Название предмета: ", fg="black", width=30).place(x=40, y=50)
        self.entry_2 = ttk.Entry(self, textvar=self.namei, width=30).place(x=210, y=50)

        self.label_2 = tk.Label(self, text="Выделенное время: ", fg="black", width=30).place(x=40, y=80)
        self.entry_2 = ttk.Entry(self, textvar=self.timei, width=30).place(x=210, y=80)

        self.btn_2 = tk.Button(self, text="Создать предмет", width="20", command=self.create, background="royalblue",
                                       foreground="white", font="15").place(x=150, y=140)

    def create(self):
        self.name = self.namei.get()
        self.time = self.timei.get()
        cursor.execute("insert into предмет (название, выделенное_время) values('" + self.name + "','" + self.time + "')")
        connection.commit()
        answer = messagebox.askyesno("Предмет добавлен успешно", "Хотите продолжить ?")
        if answer == True:
            self.destroy()
            ItemsFrame()
        else:
            self.destroy()
            Itemslist()

# Поиск студента
class UserStudent(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Поиск студента")
        self.geometry("500x500+300+100")
        self.idgroup = tk.StringVar()

        self.label_1 = tk.Label(self, text="Номер студента:", fg="black", width=20).place(x=40, y=80)
        self.entry_2 = ttk.Entry(self, textvar=self.idgroup, width=30).place(x=160, y=80)

        self.btn_3 = tk.Button(self, text="Искать студента", width="20", command=self.searche, background="royalblue",
                               foreground="white", font="15").place(x=150, y=140)
    def searche(self):

        i= self.idgroup.get()

        self.geometry("1500x500+300+100")
        self.title("Успеваемость")
        self.tree = ttk.Treeview(self, columns=(
        'Студент', 'ID группы', 'Преподаватель', 'Предмет', 'Оценка', 'Посещаемость', 'ПК', 'ППК', 'Зачет_АТТ',
        'Экзамен', 'Дата'), height=15,
                                 show='headings')
        self.tree.column('Студент', width=200)
        self.tree.column('ID группы', width=150)
        self.tree.column('Преподаватель', width=200)
        self.tree.column('Предмет', width=200)
        self.tree.column('Оценка', width=100)
        self.tree.column('Посещаемость', width=100)
        self.tree.column('ПК', width=60)
        self.tree.column('ППК', width=60)
        self.tree.column('Зачет_АТТ', width=100)
        self.tree.column('Экзамен', width=100)
        self.tree.column('Дата', width=100)

        self.tree.heading('Студент', text='Студент')
        self.tree.heading('ID группы', text='ID группы')
        self.tree.heading('Преподаватель', text='Преподаватель')
        self.tree.heading('Предмет', text='Предмет')
        self.tree.heading('Оценка', text='Оценка')
        self.tree.heading('Посещаемость', text='Посещаемость')
        self.tree.heading('ПК', text='ПК')
        self.tree.heading('ППК', text='ППК')
        self.tree.heading('Зачет_АТТ', text='Зачет_АТТ')
        self.tree.heading('Экзамен', text='Экзамен')
        self.tree.heading('Дата', text='Дата')

        cursor.execute(
            'select студенты.фио,студенты.id_группы,преподаватель.фио,предмет.название,контроль.оценка,контроль.посещаемость,контроль.пк,контроль.пкд,контроль.зачет_атт,контроль.экзамен,контроль.дата '
            'from контроль join назначение on контроль.id_назначения = назначение.id '
            'join студенты on id_студента=студенты.id '
            'join преподаватель on id_преподавателя=преподаватель.id '
            'join предмет on id_предмета=предмет.id '
            'where студенты.id=' + i + '')

        record = cursor.fetchall()
        print(record)
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(
            record[i][0], record[i][1], record[i][2], record[i][3], record[i][4], record[i][5], record[i][6],
            record[i][7], record[i][8], record[i][9], record[i][10]))
        self.tree.pack()

        self.btn_1 = tk.Button(self, text="Закрыть", command=lambda:b(), width="20", background="royalblue",
                               foreground="white", font="15").place(x=650, y=440)
        def b():
            self.destroy()
            UserStudent()

# Успеваемость
class ProgressList(tk.Toplevel):

    def __init__(self, sID: str):
        super().__init__()
        self.sID = str(sID)
        self.init_main()

    def init_main(self):
        self.title("Успеваемость")
        self.geometry("1500x500+300+100")
        self.tree = ttk.Treeview(self, columns=('Студент', 'ID группы', 'Преподаватель', 'Предмет', 'Оценка', 'Посещаемость', 'ПК', 'ППК', 'Зачет_АТТ', 'Экзамен', 'Дата'), height=15,
                                 show='headings')
        self.tree.column('Студент', width=200)
        self.tree.column('ID группы', width=150)
        self.tree.column('Преподаватель', width=200)
        self.tree.column('Предмет', width=200)
        self.tree.column('Оценка', width=100)
        self.tree.column('Посещаемость', width=100)
        self.tree.column('ПК', width=60)
        self.tree.column('ППК', width=60)
        self.tree.column('Зачет_АТТ', width=100)
        self.tree.column('Экзамен', width=100)
        self.tree.column('Дата', width=100)

        self.tree.heading('Студент', text='Студент')
        self.tree.heading('ID группы', text='ID группы')
        self.tree.heading('Преподаватель', text='Преподаватель')
        self.tree.heading('Предмет', text='Предмет')
        self.tree.heading('Оценка', text='Оценка')
        self.tree.heading('Посещаемость', text='Посещаемость')
        self.tree.heading('ПК', text='ПК')
        self.tree.heading('ППК', text='ППК')
        self.tree.heading('Зачет_АТТ', text='Зачет_АТТ')
        self.tree.heading('Экзамен', text='Экзамен')
        self.tree.heading('Дата', text='Дата')

        cursor.execute('select студенты.фио,студенты.id_группы,преподаватель.фио,предмет.название,контроль.оценка,контроль.посещаемость,контроль.пк,контроль.пкд,контроль.зачет_атт,контроль.экзамен,контроль.дата '
                       'from контроль join назначение on контроль.id_назначения = назначение.id '
                       'join студенты on id_студента=студенты.id '
                       'join преподаватель on id_преподавателя=преподаватель.id '
                       'join предмет on id_предмета=предмет.id '
                       'where студенты.id='+ self.sID + '')

        record = cursor.fetchall()
        print(record)
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0], record[i][1], record[i][2], record[i][3], record[i][4], record[i][5], record[i][6], record[i][7], record[i][8], record[i][9], record[i][10]))
        self.tree.bind("<Double-1>")
        self.tree.pack()

        self.btn_1 = tk.Button(self, text="Закрыть", width="20", command=self.b, background="royalblue",
                               foreground="white", font="15").place(x=650, y=440)

    def b(self):
        cursor.execute('select id_группы from студенты where id='+ self.sID + '')
        id=cursor.fetchone()
        print(id)
        StudentList(id[0])
        self.destroy()

# Добавление в список студентов
class StudentFrame(tk.Toplevel):
    def __init__(self, groupID: str):
        super().__init__()
        self.groupID = groupID
        self.init_main()

    def init_main(self):
        self.title("Добавление студена")
        self.geometry("500x500+300+100")
        self.name = tk.StringVar()

        self.label_1 = tk.Label(self, text="ФИО: ", fg="black", width=25
                                ).place(x=30, y=50)
        self.entry_2 = ttk.Entry(self, textvar=self.name, width=40).place(x=135, y=50)


        self.btn_2 = tk.Button(self, text="Добавить студента", width="20", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=150, y=400)

        self.btn_3 = tk.Button(self, text="Закрыть", width="20", command=self.b, background="royalblue",
                               foreground="white", font="15").place(x=150, y=440)
    def b(self):
        self.destroy()
        StudentList(self.groupID)


    def create(self):
        l = self.name.get()
        gi = str(self.groupID)
        cursor.execute("insert into студенты (фио,id_группы) values('" + l + "'," + gi + ")")
        connection.commit()
        answer = messagebox.askyesno("Студент добавлен успешно", "Хотите продолжить ?")
        if answer == True:
            self.destroy()
            StudentFrame(self.groupID)
        else:
            self.destroy()
            StudentList(self.groupID)

# Добавление в список групп
class GroupFrame(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Формирование группы")
        self.geometry("500x500+300+100")
        self.kgroup = tk.StringVar()
        self.ngroup = tk.StringVar()
        # self.idgroup = tk.StringVar()

        self.label_1=tk.Label(self, text="Курс:")
        self.label_1.place(x=120, y=80)
        self.coombobox=ttk.Combobox(self,values=[1, 2, 3, 4, 5],textvar=self.kgroup)
        self.coombobox.current(0)
        self.coombobox.place(x=160, y=80)

        self.label_1 = tk.Label(self, text="Направление:")
        self.label_1.place(x=72, y=50)
        self.coombobox = ttk.Combobox(self, values=["ИВТ", "ПИ", "Телеком", "ИБ", "БИ"], textvar=self.ngroup)
        self.coombobox.current(0)
        self.coombobox.place(x=160, y=50)



        self.btn_3 = tk.Button(self, text="Создать группу", width="20", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=150, y=380)

        self.btn_3 = tk.Button(self, text="Закрыть", width="20", command=self.de, background="royalblue",
                               foreground="white", font="15").place(x=150, y=430)
    def de(self):
        self.destroy()
        GroupList()


    def create(self):
        k = self.kgroup.get()
        n = self.ngroup.get()
        cursor.execute("insert into группы (курс,направление) values('" + k + "','" + n + "')")
        connection.commit()
        answer = messagebox.askyesno("Группа добавлена успешно","Хотите продолжить ?")
        if answer == True:
            self.destroy()
            GroupFrame()
        else:
            self.destroy()
            GroupList()

# Запрос списка групп
class GroupList(tk.Toplevel):

    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Группы")
        self.geometry("500x500+300+100")

        self.tree = ttk.Treeview(self, columns=('ID группы', 'курс', 'направление'), height=15, show='headings')
        self.tree.column('ID группы', width=100)
        self.tree.column('курс', width=100)
        self.tree.column('направление', width=100)

        self.tree.heading('ID группы', text='ID группы')
        self.tree.heading('курс', text='курс')
        self.tree.heading('направление', text='направление')
        cursor.execute('select * from группы')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0], record[i][1], record[i][2]))
        self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()
        self.btn_3 = tk.Button(self, text="создать новую группу", width="20", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=150, y=380)

        self.btn_4 = tk.Button(self, text="Назад", width="20",command=self.up, background="royalblue",
                               foreground="white", font="15").place(x=150, y=430)

    def up(self):
        self.destroy()
        Menu()

    def create(self):
        self.destroy()
        GroupFrame()


    def link_tree(self, event):
        curItem = self.tree.focus()
        id = self.tree.item(curItem)['values'][0]
        print(id)
        StudentList(id)
        self.destroy()

# Запрос списка студентов
class StudentList(tk.Toplevel):
    """docstring for List"""

    def __init__(self, groupID: str):
        super().__init__()
        self.groupID = str(groupID)
        self.init_main()

    def init_main(self):
        self.title("Студенты")
        self.geometry("500x500+300+100")
        self.tree = ttk.Treeview(self, columns=('ID','ID группы', 'ФИО'), height=15,
                                 show='headings')
        self.tree.column('ID', width=40)
        self.tree.column('ID группы', width=40)
        self.tree.column('ФИО', width=200)


        self.tree.heading('ID', text='ID')
        self.tree.heading('ID группы', text='ID группы')
        self.tree.heading('ФИО', text='ФИО')


        cursor.execute(
            'select*from студенты where id_группы = ' + self.groupID + '')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0], record[i][1], record[i][2]))
        self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()

        self.btn_1 = tk.Button(self, text="Добавить студента", width="20", command=self.add, background="royalblue",
                               foreground="white", font="15").place(x=150, y=380)

        self.btn_2 = tk.Button(self, text="Закрыть", width="20", command=self.b, background="royalblue",
                               foreground="white", font="15").place(x=150, y=430)

    def b(self):
        self.destroy()
        GroupList()

    def link_tree(self, event):
        curItem = self.tree.focus()
        id = self.tree.item(curItem)['values'][0]
        print(id)
        ProgressList(id)
        self.destroy()

    def add(self):
        self.destroy()
        StudentFrame(self.groupID)

class Contorl(tk.Toplevel):

    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):

        self.destroy()
        AppointmentsList()
        GroupList()
        ContorlFrame()

# Конторль
class ContorlFrame(tk.Toplevel):

    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Контроль")
        self.geometry("500x500+300+100")
        self.ids = tk.StringVar()
        self.idn = tk.StringVar()
        self.ass = tk.StringVar()
        self.nb = tk.StringVar()
        self.pk = tk.StringVar()
        self.pkk = tk.StringVar()
        self.att = tk.StringVar()
        self.eg = tk.StringVar()
        self.date = tk.StringVar()


        self.label_1 = tk.Label(self, text="ID Студента: ", fg="black", width=30).place(x=50, y=20)
        self.entry_2 = ttk.Entry(self, textvar=self.ids, width=30).place(x=210, y=20)

        self.label_2 = tk.Label(self, text="ID Назначения: ", fg="black", width=30).place(x=40, y=50)
        self.entry_2 = ttk.Entry(self, textvar=self.idn, width=30).place(x=210, y=50)

        self.label_3 = tk.Label(self, text="Оценка:")
        self.label_3.place(x=140, y=80)
        self.coombobox = ttk.Combobox(self, values=[1, 2, 3, 4, 5], textvar=self.ass)
        self.coombobox.current(0)
        self.coombobox.place(x=210, y=80)

        self.rbtn_1 = tk.Radiobutton(self, text="Был на занятии", width="20", value="был",
                                     variable=self.nb).place(x=100, y=110)
        self.rbtn_2 = tk.Radiobutton(self, text="Не был на занятии", width="25", value="небыл",
                                     variable=self.nb).place(x=250, y=110)

        self.label_5 = tk.Label(self, text="8 неделя:")
        self.label_5.place(x=140, y=140)
        self.coombobox = ttk.Combobox(self, values=[1, 2, 3, 4, 5], textvar=self.pk)
        self.coombobox.current(0)
        self.coombobox.place(x=210, y=140)

        self.label_6 = tk.Label(self, text="16 неделя:")
        self.label_6.place(x=120, y=165)
        self.coombobox = ttk.Combobox(self, values=[1, 2, 3, 4, 5], textvar=self.pkk)
        self.coombobox.current(0)
        self.coombobox.place(x=210, y=165)

        self.label_6 = tk.Label(self, text="зачет-аттестация:")
        self.label_6.place(x=120, y=190)
        self.coombobox = ttk.Combobox(self, values=[1, 2, 3, 4, 5], textvar=self.att)
        self.coombobox.current(0)
        self.coombobox.place(x=210, y=190)

        self.label_6 = tk.Label(self, text="экзамен:")
        self.label_6.place(x=120, y=215)
        self.coombobox = ttk.Combobox(self, values=[1, 2, 3, 4, 5], textvar=self.eg)
        self.coombobox.current(0)
        self.coombobox.place(x=210, y=215)


        self.label_9 = tk.Label(self, text="дата: ", fg="black", width=30).place(x=40, y=240)
        self.entry_9 = ttk.Entry(self, textvar=self.date, width=30).place(x=210, y=240)

        self.btn_2 = tk.Button(self, text="Готово", width="20", command=self.create, background="royalblue",
                               foreground="white", font="15").place(x=150, y=440)

    def create(self):
        self.s = self.ids.get()
        self.n = self.idn.get()
        self.a = self.ass.get()
        self.b = self.nb.get()
        self.k = self.pk.get()
        self.kk = self.pkk.get()
        self.tt = self.att.get()
        self.e = self.eg.get()
        self.d = self.date.get()

        cursor.execute(
            "insert into контроль (id_студента, id_назначения,оценка,посещаемость,пк,пкд,зачет_атт,экзамен,дата) "
            "values('" + self.s + "','" + self.n + "','" + self.a + "','" + self.b + "','" + self.k + "','" + self.kk + "','" + self.tt + "','" + self.e + "','" + self.d + "')")
        connection.commit()
        answer = messagebox.askyesno("Назначение добавлен успешно", "Хотите продолжить ?")
        if answer == True:
            self.destroy()
            ContorlFrame()
        else:
            self.destroy()
            Menu()


if __name__ == "__main__":
    root = tk.Tk()
    root.title("Institute Education Process")
    root.geometry("500x500+300+100")
    app = Main(root)
    app.pack()
    root.mainloop()
